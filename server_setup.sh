#!/bin/bash

##########################################################################################################################
#/*       _\|/_                                                                                                          #
#         (o o)                                                                                                          #
# +----oOO-{_}-OOo------------------------------------------------------------------------------------------------------+#
# |                            OpenVPN demo server setup                                                                |#
# |                            #############################################################                            |#
# |                            Repository:                                                                              |#
# |                             https://gitlab.kit.edu/kim-gabriel.deckert/dhbw-openvpn-demo                            |#
# |                                                                                                                     |#
# |                            Authors:                                                                                 |#
# |                             Andreas Alexopoulos                                                                     |#
# |                             Kim-Gabriel Deckert (deckert@kit.edu)                                                   |#
# |                             Linus Schmidt                                                                           |#
# |                             Samuel Gesk                                                                             |#
# |                                                                                                                     |#
# |                             DHBW Karlsruhe, April 2024                                                              |#
# |                            #############################################################                            |#
# |                            System: CentOS Stream 9 running on HyperV                                                |#
# +--------------------------------------------------------------------------------------------------------------------*/#
##########################################################################################################################

### preparation
# check if run by root
if [ "${USER}" != "root" ]; then
  echo "must be run as root"
  exit 1
fi

# fetch vars
varsFile="server_vars"
test -r "${varsFile}" || exit 1
source "${varsFile}"

# config variables
export EASYRSA_PKI="${pathPki}"
export EASYRSA_DN="${confCaDn}"
export EASYRSA_KEY_SIZE="${confCaKeysize}"
export networkEthernetIf hostDomain hostFqdn networkEthernetIp4 networkEthernetPref4 networkEthernetGw4 networkEthernetDns41 \
  networkEthernetDns42 networkEthernetIp6 networkEthernetPref6 networkEthernetGw6 networkEthernetDns61 networkEthernetDns62 \
  networkIp4Net networkIp4Bridge networkIp4Prefix networkIp4Netmask networkIp6Net networkIp6Bridge networkIp6Prefix \
  networkBridgeInterface pathDomainHostsFile pathOpenVpnAuth pathOpenVpnCerts pathOpenVpnClientsConfigs pathOpenVpnLog \
  pathOpenVpnScripts pathLdapConf pathLdapData ldapBaseDn ldapAdminDn ldapAdminPassword ldapSrvAcc ldapSrvPass ldapUsersDn

### basic os setup
# disable firewalld
systemctl stop firewalld
systemctl disable firewalld

# enable epel-repo
yum install epel-release

# install packages
xargs yum -y install <<<"${packages[@]}"

# create openvpn directories
for dir in ${openvpnDirs[@]}; do
  test -d "${dir}" || mkdir "${dir}"
done

# install mustache for template rendering
test -x "${pathMustacheBash}" || {
  tmpfile="$(mktemp)" &&
    curl -sSL https://raw.githubusercontent.com/tests-always-included/mo/master/mo -o "${tmpfile}" &&
    mv "${tmpfile}" "${pathMustacheBash}" &&
    chmod +x "${pathMustacheBash}"
}

# render config files
for configfile in ${configfiles[@]}; do
  localfile="config/$(cut -d '|' -f 1 <<<"${configfile}")"
  configPath="$(cut -d '|' -f 2 <<<"${configfile}")"
  "${pathMustacheBash}" -u <"${localfile}" | tr -d '\r' >"${configPath}"
done

# reload kernel params
sysctl -p

# reload network config
nmcli connection reload
nmcli connection up "${networkEthernetIf}"

# add dirs to context
semanage fcontext -a -t openvpn_etc_rw_t "${pathOpenVpnMisc}(/.*)"
semanage fcontext -a -t slapd_etc_t "${pathLdapConf}/slapd\.conf"
semanage fcontext -a -t dnsmasq_etc_t "${pathDomainHostsFile/./\\.}"

# load iptables config
iptables-restore <"/etc/sysconfig/iptables"
ip6tables-restore <"/etc/sysconfig/ip6tables"

# ca setup
test -L "/usr/local/bin/easyrsa" || ln -s /usr/share/easy-rsa/3/easyrsa /usr/local/bin/
test -d "${pathPkiBase}" || mkdir "${pathPkiBase}"

# openvpn permission setup
chown -R root:openvpn "${pathOpenVpnBase}"
chmod -R u+x "${pathOpenVpnScripts}"
restorecon -r "${pathOpenVpnBase}"
restorecon -r "${pathOpenVpnLog}"
setsebool -P openvpn_run_unconfined 1
semanage permissive -a openvpn_unconfined_script_t

### ca operations
# build ca
test -d "${pathPki}" || {
  /usr/local/bin/easyrsa init-pki && echo "${confCaName}" | /usr/local/bin/easyrsa build-ca nopass
  openssl dhparam -out "${pathOpenVpnCerts}/dh2048.pem" 2048
  openvpn --genkey tls-auth "${pathOpenVpnCerts}/ta.key"
}

# update system trust
test -L "/etc/pki/ca-trust/source/anchors/${hostName}_ca.crt" || ln -s "${pathPki}/ca.crt" \
  "/etc/pki/ca-trust/source/anchors/${hostName}_ca.crt"
/usr/bin/update-ca-trust

# generate server certificate
echo "yes" | /usr/local/bin/easyrsa --san="DNS:localhost" build-server-full "${hostFqdn}" nopass &&
  cp "${pathPki}/issued/${hostFqdn}.crt" "${pathOpenVpnCerts}/" &&
  cp "${pathPki}/private/${hostFqdn}.key" "${pathOpenVpnCerts}/" &&
  cp "${pathPki}/ca.crt" "${pathOpenVpnCerts}/"

# generate user certificates
for ldapUser in ${ldapUsers[@]}; do
  username="$(cut -d '|' -f 1 <<<"${ldapUser}")"
  grep -q "${username}"'$' <(cut -d $'\t' -f 6 <"${pathPki}/index.txt") ||
    echo "yes" | /usr/local/bin/easyrsa build-client-full "${username}" nopass &&
    cp "${pathPki}/issued/${username}.crt" "${pathOpenVpnCerts}/" &&
    cp "${pathPki}/private/${username}.key" "${pathOpenVpnCerts}/"
done

# copy generated certificates to openvpn dir
cp "${pathPki}/issued/${hostFqdn}.crt" "${pathOpenVpnCerts}/" &&
  cp "${pathPki}/private/${hostFqdn}.key" "${pathOpenVpnCerts}/" &&
  cp "${pathPki}/ca.crt" "${pathOpenVpnCerts}/"
cp "${pathPki}/issued/$(cut -d '|' -f 1 <<<"${ldapUsers[0]}").crt" "${pathOpenVpnCerts}/" &&
  cp "${pathPki}/private/$(cut -d '|' -f 1 <<<"${ldapUsers[0]}").key" "${pathOpenVpnCerts}/"

### ldap operations
# grant access to configuration files
chown -R ldap:ldap "${pathLdapConf}"
restorecon -r "${pathLdapConf}"

# flush ldap database
test -d "${pathLdapData}" && rm -rf "${pathLdapData}"
mkdir "${pathLdapData}"
chown ldap:ldap "${pathLdapData}"
restorecon -r "${pathLdapData}"

# reload systemd daemons
systemctl daemon-reload

# restart slapd service
systemctl restart slapd

# create base directory tree
echo "dn: ${ldapBaseDn}
objectclass: dcObject
objectclass: organization
o: ${ldapUsersCn}
dc: ${ldapBaseCn}

dn: ${ldapUsersDn}
objectclass: dcObject
objectclass: organization
o: ${ldapUsersCn}
dc: ${ldapUsersCn}
" | ldapadd -x -H "ldap://127.0.0.1/" -D "${ldapAdminDn}" -w "${ldapAdminPassword}"

# create tempfile for collecting dns entries
addnhosts="$(mktemp)"

# generate user specific configuration files
for ldapUser in ${ldapUsers[@]}; do
  # parse user info from array
  username="$(cut -d '|' -f 1 <<<"${ldapUser}")"
  userpw="$(cut -d '|' -f2 <<<"${ldapUser}")"

  # create ldap user and set password
  echo "
dn: cn=${username},${ldapUsersDn}
objectClass: top
objectClass: person
objectClass: organizationalPerson
sn: ${username}
userPassword: {crypt}x
" | ldapadd -x -H "ldap://127.0.0.1/" -D "${ldapAdminDn}" -w "${ldapAdminPassword}" &&
    ldappasswd -x -H "ldap://127.0.0.1/" -D "${ldapAdminDn}" -w "${ldapAdminPassword}" \
      -x "cn=${username},${ldapUsersDn}" -s "${userpw}"

  # config from vars
  export ipv4="$(cut -d '|' -f 3 <<<"${ldapUser}")"
  export ipv6="$(cut -d '|' -f 4 <<<"${ldapUser}")"
  export ca="$(cat "${pathOpenVpnCerts}/ca.crt")"
  export tlsauth="$(cat "${pathOpenVpnCerts}/ta.key")"
  export cert="$(awk -s '/-----BEGIN CERTIFICATE-----/,/-----END CERTIFICATE-----/' \
    <"${pathOpenVpnCerts}/${username}.crt")"
  export key="$(cat "${pathOpenVpnCerts}/${username}.key")"

  # render template of client specific config (openvpn client)
  "${pathMustacheBash}" -o='@{@' -c='@}@' -u <"${pathTplClient}" | tr -d '\r' >"${pathOpenVpnClient}/${username}.ovpn"

  # render template of client specific config (openvpn server)
  "${pathMustacheBash}" -o='@{@' -c='@}@' -u <"${pathTplCcd}" | tr -d '\r' >"${pathOpenVpnClientsConfigs}/${username}"

  # generate dns entries
  echo "${ipv4} ${username} ${username}.${hostFqdn}" >>"${addnhosts}"
  echo "${ipv6} ${username} ${username}.${hostFqdn}" >>"${addnhosts}"
done

### dnsmasq installation
# install dns entries
mv "${addnhosts}" "${pathDomainHostsFile}"
chmod a=r "${pathDomainHostsFile}"

# grant access to dns entries by dnsmasq
restorecon "${pathDomainHostsFile}"

# enable and start openvpn server instances
systemctl start openvpn-server@udp4.service openvpn-server@tcp4.service openvpn-server@udp6.service \
  openvpn-server@tcp6.service
systemctl enable openvpn-server@udp4.service openvpn-server@tcp4.service openvpn-server@udp6.service \
  openvpn-server@tcp6.service

# ensure services are enabled and reloaded
systemctl restart dnsmasq iptables ip6tables dnsmasq fail2ban
systemctl enable sshd fail2ban slapd iptables ip6tables dnsmasq
