## general vars
hostName="$(hostname)"
hostDomain="$(hostname -d)"
hostFqdn="$(hostname -f)"
hostParent="$(cut -d '.' -f 1 <<<"${hostDomain}")"

## network config
networkEthernetIf="eth0"

networkEthernetIp4="129.13.223.143"
networkEthernetPref4=24
networkEthernetGw4="129.13.223.1"
networkEthernetDns41="129.13.64.5"
networkEthernetDns42="141.52.3.3"

networkEthernetIp6="2a00:1398:4:c802::810d:df8f"
networkEthernetPref6=64
networkEthernetGw6="2a00:1398:4:c802::1"
networkEthernetDns61="2a00:1398::1"
networkEthernetDns62="2a00:1398::2"

networkIp4Net="172.25.0.0"
networkIp4Bridge="172.25.0.1"
networkIp4Prefix=24
networkIp4Netmask="255.255.255.0"
networkIp6Net="fd00::1"
networkIp6Bridge="fd00::1"
networkIp6Prefix=64
networkBridgeInterface="vpnbridge"

## ca config
confCaName="${hostName} CA"
confCaDn="cn_only"
confCaKeysize=4096

## path to mustache executable
pathMustacheBash="/usr/local/bin/mustache"

## common openvpn paths
pathOpenVpnBase="/etc/openvpn"
pathOpenVpnLog="/var/log/openvpn"
pathOpenVpnAuth="${pathOpenVpnBase}/auth"
pathOpenVpnCerts="${pathOpenVpnBase}/certs"
pathOpenVpnClient="${pathOpenVpnBase}/client"
pathOpenVpnClientsConfigs="${pathOpenVpnBase}/users"
pathOpenVpnMisc="${pathOpenVpnBase}/misc"
pathOpenVpnScripts="${pathOpenVpnBase}/scripts"
pathOpenVpnServer="${pathOpenVpnBase}/server"
openvpnDirs=("${pathOpenVpnLog}" "${pathOpenVpnAuth}" "${pathOpenVpnCerts}" "${pathOpenVpnClient}" "${pathOpenVpnClientsConfigs}" "${pathOpenVpnMisc}" "${pathOpenVpnScripts}" "${pathOpenVpnServer}")

## paths to openvpn templates
pathTplCcd="${pathOpenVpnMisc}/ccd.tpl"
pathTplClient="${pathOpenVpnMisc}/client.tpl"

## ca paths
pathPkiBase="/opt/ca"
pathPki="${pathPkiBase}/pki"

## ldap paths
pathLdapConf="/etc/openldap"
pathLdapData="/var/lib/openldap-data"

## path to the file containing dns entries
pathDomainHostsFile="/etc/hosts.${hostName}"

## ldap
ldapBaseCn="${hostParent}"
ldapBaseDn="dc=$(sed 's#\.#,dc=#g' <<<"${hostDomain}")"
ldapUsersCn="${hostName}"
ldapUsersDn="dc=${ldapUsersCn},${ldapBaseDn}"
ldapAdminUser="ldapadmin"
ldapAdminDn="cn=${ldapAdminUser},${ldapBaseDn}"
ldapAdminPassword="<password>"
# at the first position must be openvpnsrv, format: username|password|ipv4|ipv6
ldapUsers=("openvpnsrv|<password>|127.0.0.1|::1" "laptop1|<password>|172.25.0.101|fd00::65" "laptop2|<password>|172.25.0.102|fd00::66" "raspi1|<password>|172.25.0.201|fd00::c9" "raspi2|<password>|172.25.0.202|fd00::ca")
ldapSrvAcc="$(cut -d '|' -f 1 <<<"${ldapUsers[0]}")"
ldapSrvPass="$(cut -d '|' -f 2 <<<"${ldapUsers[0]}")"

## required yum/dnf packages
packages=("fail2ban" "iptables-services" "bridge-utils" "easy-rsa" "openvpn" "openvpn-auth-ldap" "openldap-servers" "openldap-clients" "dnsmasq")

## config files
configfiles=("ccd.tpl|${pathTplCcd}" "client.tpl|${pathTplClient}" "dnsmasq.conf|/etc/dnsmasq.conf" "eth0.nmconnection|/etc/NetworkManager/system-connections/${networkEthernetIf}.nmconnection" "fail2ban_jail_sshd.conf|/etc/fail2ban/jail.d/20-sshd.conf" "iptables|/etc/sysconfig/iptables" "ip6tables|/etc/sysconfig/ip6tables" "ldap.conf|${pathLdapConf}/ldap.conf" "openvpn_auth-ldap.conf|${pathOpenVpnAuth}/ldap.conf" "openvpn_server_tap-tcp4.conf|${pathOpenVpnServer}/tcp4.conf" "openvpn_server_tap-tcp6.conf|${pathOpenVpnServer}/tcp6.conf" "openvpn_server_tap-udp4.conf|${pathOpenVpnServer}/udp4.conf" "openvpn_server_tap-udp6.conf|${pathOpenVpnServer}/udp6.conf" "resolv.conf|/etc/resolv.conf" "server_down.sh|${pathOpenVpnScripts}/server_down.sh" "server_up.sh|${pathOpenVpnScripts}/server_up.sh" "slapd.conf|${pathLdapConf}/slapd.conf" "slapd.service|/etc/systemd/system/slapd.service" "sysctl.conf|/etc/sysctl.conf")
