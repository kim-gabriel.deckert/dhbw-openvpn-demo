#!/bin/bash
tundev="${1}"
bridgeInterface="{{networkBridgeInterface}}"

/usr/bin/grep -q "master ${bridgeInterface}" <(/usr/sbin/ip link show dev "${tundev}" </usr/sbin/ip) && /usr/sbin/ip link set dev "${tundev}" nomaster
