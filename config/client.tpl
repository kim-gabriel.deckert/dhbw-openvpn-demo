client
dev tap
remote {{hostFqdn}} 1194 udp6
remote {{hostFqdn}} 1194 tcp6-client
remote {{hostFqdn}} 1194 udp4
remote {{hostFqdn}} 1194 tcp4-client
resolv-retry infinite
persist-tun

auth-user-pass
remote-cert-tls server
key-direction 1

<ca>
@{@ca@}@
</ca>

<cert>
@{@cert@}@
</cert>

<key>
@{@key@}@
</key>

<tls-auth>
@{@tlsauth@}@
</tls-auth>

verb 3
