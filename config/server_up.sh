#!/bin/bash
tundev="${1}"
bridgeInterface="{{networkBridgeInterface}}"
ipv4="{{networkIp4Bridge}}/{{networkIp4Prefix}}"
ipv6="{{networkIp6Bridge}}/{{networkIp6Prefix}}"

/usr/sbin/ip link show dev "${bridgeInterface}" &>/dev/null || {
  /usr/sbin/ip link add name "${bridgeInterface}" type bridge
  /usr/sbin/ip link set dev "${bridgeInterface}" up
}
/usr/sbin/ip link set dev "${tundev}" master "${bridgeInterface}"

/usr/sbin/ip address delete "${ipv4}" dev "${tundev}" &&
  /usr/sbin/ip address replace "${ipv4}" dev "${bridgeInterface}"
/usr/sbin/ip address delete "${ipv6}" dev "${tundev}" &&
  /usr/sbin/ip address replace "${ipv6}" dev "${bridgeInterface}"
